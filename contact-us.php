
<!DOCTYPE html>
<!--[if IE 8]>          <html class="ie ie8"> <![endif]-->
<!--[if IE 9]>          <html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->  <html> <!--<![endif]-->
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <title>Jaali - Contact Us</title>       
        <meta name="keywords" content="Jaali" />
        <meta name="description" content="Jaali">
        <meta name="author" content="Jaali.in">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- Web Fonts  -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <!-- Libs CSS -->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.css">
        <link rel="stylesheet" href="vendor/owl-carousel/owl.carousel.css" media="screen">
        <link rel="stylesheet" href="vendor/owl-carousel/owl.theme.css" media="screen">
        <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css" media="screen">
        <link rel="stylesheet" href="vendor/isotope/jquery.isotope.css" media="screen">
        <link rel="stylesheet" href="vendor/mediaelement/mediaelementplayer.css" media="screen">

        <!-- Theme CSS -->
        <link rel="stylesheet" href="css/theme.css">
        <link rel="stylesheet" href="css/theme-elements.css">
        <link rel="stylesheet" href="css/theme-blog.css">
        <link rel="stylesheet" href="css/theme-shop.css">
        <link rel="stylesheet" href="css/theme-animate.css">

        <!-- Responsive CSS -->
        <link rel="stylesheet" href="css/theme-responsive.css" />

        <!-- Skin CSS -->
        <link rel="stylesheet" href="css/skins/default.css">

        <!-- Custom CSS -->
        <link rel="stylesheet" href="css/custom.css">

        <!-- Head Libs -->
        <script src="vendor/modernizr.js"></script>

        <!--[if IE]>
            <link rel="stylesheet" href="css/ie.css">
        <![endif]-->

        <!--[if lte IE 8]>
            <script src="vendor/respond.js"></script>
        <![endif]-->

    </head>
    <body class="boxed">
        <div class="body">
            <header id="header">
                <div class="container">
                    <h1 class="logo">
                        <a href="index.html">
                            <img alt="Jaali" width="153" height="96" data-sticky-width="82" data-sticky-height="10" src="img/logo.png">
                        </a>
                    </h1>
                    
                    <button class="btn btn-responsive-nav btn-inverse" data-toggle="collapse" data-target=".nav-main-collapse">
                        <i class="icon icon-bars"></i>
                    </button>
                </div>
                <div class="navbar-collapse nav-main-collapse collapse">
                    <div class="container">
                        <ul class="social-icons">
                            <li class="facebook"><a href="http://www.facebook.com/" target="_blank" title="Facebook">Facebook</a></li>
                            <li class="twitter"><a href="http://www.twitter.com/" target="_blank" title="Twitter">Twitter</a></li>
                            <li class="googleplus"><a href="http://plus.google.com/" target="_blank" title="Google+">Google+</a></li>
                        </ul>
                        <nav class="nav-main mega-menu">
                            <ul class="nav nav-pills nav-main" id="mainMenu">
                                <li>
                                    <a href="index.html">Home</a>
                                </li>
                                <li>
                                    <a href="jaali.html">Jaali Designs</a>
                                </li>
                                <li>
                                    <a href="concepts.html">Concepts</a>
                                </li>
                                <li class="active">
                                    <a href="contact-us.php">Contact Us</a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </header>

            <div role="main" class="main">

                <section class="page-top" style="margin-bottom: 0px;">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Contact Us</h2>
                            </div>
                        </div>
                    </div>
                </section>

                <!-- Google Maps -->
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d236.46966632871016!2d73.87758325685225!3d18.505630682204504!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x5eff077fed9fe9b!2sJaali.in+(+Sign-O-Craft+)!5e0!3m2!1sen!2sin!4v1411477895831" width="100%" height="450" frameborder="0" style="border:0"></iframe>

                <div class="container">

                    <div class="row">
                        <div class="col-md-6">

                            <div class="offset-anchor" id="contact-sent"></div>

                            
                            <h2 class="short"><strong>Contact</strong> Us</h2>
                            <form id="contactForm" action="contact-us.php#contact-sent" method="POST" enctype="multipart/form-data" data-type="advanced">
                                <input type="hidden" value="true" name="emailSent" id="emailSent">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-6">
                                            <label>Your name *</label>
                                            <input type="text" value="" data-msg-required="Please enter your name." maxlength="100" class="form-control" name="name" id="name">
                                        </div>
                                        <div class="col-md-6">
                                            <label>Your email address *</label>
                                            <input type="email" value="" data-msg-required="Please enter your email address." data-msg-email="Please enter a valid email address." maxlength="100" class="form-control" name="email" id="email">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Subject</label>
                                            <select data-msg-required="Please enter the subject." class="form-control" name="subject" id="subject">
                                                <option value=""></option>
                                                <option value="Option 1">Request a Quote</option>
                                                <option value="Option 2">Information</option>
                                                <option value="Option 3">Help & Support</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Attachment</label>
                                            <input type="file" name="attachment" id="attachment">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <label>Message *</label>
                                            <textarea maxlength="5000" data-msg-required="Please enter your message." rows="10" class="form-control" name="message" id="message"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Human Verification *</label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4">
                                            <div class="captcha form-control">
                                                <div class="captcha-image">
                                                    <img src="php/simple-php-captcha/simple-php-captcha.php/?_CAPTCHA&amp;t=0.96033400+1455612924" alt="CAPTCHA code">                                              </div>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" value="" maxlength="6" data-msg-captcha="Wrong verification code." data-msg-required="Please enter the verification code." placeholder="Type the verification code." class="form-control" name="captcha" id="captcha">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <input type="submit" id="contactFormSubmit" value="Send Message" class="btn btn-primary btn-lg pull-right" data-loading-text="Loading...">
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">

                            <h4>The <strong>Office</strong></h4>
                            <ul class="list-unstyled">
                                <li><i class="icon icon-map-marker"></i> <strong>Address:</strong> #13, Iqra Building, New Modikhana, Pune, Maharashtra, India, 411001</li>
                                <li><i class="icon icon-phone"></i> <strong>Phone:</strong> +91 99238 08585</li>
                                <li><i class="icon icon-phone"></i> <strong>Mobile:</strong> +91 99238 08989</li>
                                <li><i class="icon icon-envelope"></i> <strong>Email:</strong> <a href="mailto:jaali.in@gmail.com">jaali.in@gmail.com</a></li>
                            </ul>

                            <hr />

                            <h4>Business <strong>Hours</strong></h4>
                            <ul class="list-unstyled">
                                <li><i class="icon icon-time"></i> Monday - Friday: 10am to 8pm</li>
                                <li><i class="icon icon-time"></i> Saturday: 10am to 8pm</li>
                                <li><i class="icon icon-time"></i> Sunday: Closed</li>
                            </ul>

                        </div>
                    </div>

                </div>

            </div>

            <footer id="footer">
                <div class="container">
                    <div class="row">
                        
                        <div class="col-md-6">
                            <div class="newsletter">
                                <h4>About Us</h4>
                                <p>Jaali.in is a manufacturer of custom-made and made-to-measure Jaali designs using modern CNC machines based in Pune, India.</p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="contact-details">
                                <h4>Contact Us</h4>
                                <ul class="contact">
                                    <li><p><i class="icon icon-map-marker"></i> <strong>Address:</strong> #13, Iqra Building, New Modikhana, Pune, Maharashtra, India, 411001</p></li>
                                    <li><p><i class="icon icon-phone"></i> <strong>Phone:</strong> +91 99238 08585</p></li>
                                    <li><p><i class="icon icon-phone"></i> <strong>Mobile:</strong> +91 99238 08989</p></li>
                                    <li><p><i class="icon icon-envelope"></i> <strong>Email:</strong> <a href="mailto:jaali.in@gmail.com">jaali.in@gmail.com</a></p></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="index.html" class="logo">
                                    <img alt="Jaali.in" class="img-responsive" src="img/logo-footer.png">
                                </a>
                            </div>
                            <div class="col-md-7">
                                <p style="color:#E2E2E2;">© Copyright 2014. All Rights Reserved. Created by <a href="http://www.ibbisoft.com">Ibbi Software Inc.</a></p>
                            </div>
                            <div class="col-md-4">
                                <nav id="sub-menu">
                                    <ul>
                                        <img alt="Sign-O-Craft" class="img-responsive" src="img/Signocraft_Logo.png">
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div>

        <!-- Libs -->
        <script src="vendor/jquery.js"></script>
        <script src="vendor/jquery.appear.js"></script>
        <script src="vendor/jquery.easing.js"></script>
        <script src="vendor/jquery.cookie.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.js"></script>
        <script src="vendor/jquery.validate.js"></script>
        <script src="vendor/jquery.stellar.js"></script>
        <script src="vendor/jquery.knob.js"></script>
        <script src="vendor/jquery.gmap.js"></script>
        <script src="vendor/twitterjs/twitter.js"></script>
        <script src="vendor/isotope/jquery.isotope.js"></script>
        <script src="vendor/owl-carousel/owl.carousel.js"></script>
        <script src="vendor/jflickrfeed/jflickrfeed.js"></script>
        <script src="vendor/magnific-popup/magnific-popup.js"></script>
        <script src="vendor/mediaelement/mediaelement-and-player.js"></script>
        
        <!-- Theme Initializer -->
        <script src="js/theme.plugins.js"></script>
        <script src="js/theme.js"></script>

        <!-- Current Page JS -->
        <script src="js/views/view.contact.js"></script>
        
        <!-- Custom JS -->
        <script src="js/custom.js"></script>

        <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
        <script>

            /*
            Map Settings

                Find the Latitude and Longitude of your address:
                    - http://universimmedia.pagesperso-orange.fr/geo/loc.htm
                    - http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

            */

            // Map Markers
            var mapMarkers = [{
                address: "2271, Hidayatulla Road, New Modikhana, Katad Khana, Pune, Maharashtra 411001, India",
                html: "<strong>India Office</strong><br>#13, Iqra Building, New Modikhana, Pune, Maharashtra, India, 411001<br><br><a href='#' onclick='mapCenterAt({latitude: 18.50561427109083, longitude: 73.87758493423462, zoom: 20}, event)'>[+] zoom here</a>",
                icon: {
                    image: "img/pin.png",
                    iconsize: [26, 46],
                    iconanchor: [12, 46]
                }
            }];

            // Map Initial Location
            var initLatitude = 18.50561427109083;
            var initLongitude = 73.87758493423462;

            // Map Extended Settings
            var mapSettings = {
                controls: {
                    panControl: true,
                    zoomControl: true,
                    mapTypeControl: true,
                    scaleControl: true,
                    streetViewControl: true,
                    overviewMapControl: true
                },
                scrollwheel: false,
                markers: mapMarkers,
                latitude: initLatitude,
                longitude: initLongitude,
                zoom: 20
            };

            var map = $("#googlemaps").gMap(mapSettings);

            // Map Center At
            var mapCenterAt = function(options, e) {
                e.preventDefault();
                $("#googlemaps").gMap("centerAt", options);
            }

        </script>

        <!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information.
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-22826831-1']);
  _gaq.push(['_setDomainName', 'jaali.in']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script
         -->

    </body>
</html>
